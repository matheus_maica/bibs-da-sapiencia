package com.JPASpringProject.JPA.service;

import com.JPASpringProject.JPA.model.Autor;
import com.JPASpringProject.JPA.repository.AutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AutorService {
    @Autowired
    private AutorRepository autorRepository;

    public List<Autor> listarAutors() {
        return autorRepository.findAll();
    }

    public Autor adicionarAutor(Autor autor) {
        return autorRepository.save(autor);
    }

    public Autor validarAutor(Long id){
        Optional<Autor> autor = autorRepository.findById(id);
        if(autor.isEmpty()){
            throw new IllegalArgumentException();
        }
        return autor.get();
    }
}
