package com.JPASpringProject.JPA.service;

import com.JPASpringProject.JPA.model.Autor;
import com.JPASpringProject.JPA.model.Livro;
import com.JPASpringProject.JPA.repository.AutorRepository;
import com.JPASpringProject.JPA.repository.LivroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LivroService {
    @Autowired
    private LivroRepository livroRepository;
    @Autowired
    private AutorService autorService;

    public List<Livro> listarLivros() {
        return livroRepository.findAll();
    }

    public Livro adicionarLivro(Livro livro) {
        return livroRepository.save(livro);
    }

    public Livro validarLivro(Long id){
        Optional<Livro> livro = livroRepository.findById(id);
        if(livro.isEmpty()){
            throw new IllegalArgumentException();
        }
        return livro.get();
    }

    public Livro vincularAutor(Long autor_id, Long livro_id) {
        Autor autor = autorService.validarAutor(autor_id) ;
        Livro livro =validarLivro(livro_id);
        livro.setAutor(autor);
        return livroRepository.save(livro);
    }
}
