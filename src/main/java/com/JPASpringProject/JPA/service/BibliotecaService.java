package com.JPASpringProject.JPA.service;

import com.JPASpringProject.JPA.model.Biblioteca;
import com.JPASpringProject.JPA.model.Livro;
import com.JPASpringProject.JPA.repository.BibliotecaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BibliotecaService {
    @Autowired
    private BibliotecaRepository bibliotecaRepository;

    @Autowired
    private LivroService livroService;

    public List<Biblioteca> listarBibliotecas() {
        return bibliotecaRepository.findAll();
    }

    public Biblioteca adicionarBiblioteca(Biblioteca biblioteca) {
        return bibliotecaRepository.save(biblioteca);
    }

    private Biblioteca validarBiblioteca(Long id) {
        Optional<Biblioteca> biblioteca = bibliotecaRepository.findById(id);
        if(biblioteca.isEmpty()){
            throw new IllegalArgumentException();
        }
        return biblioteca.get();
    }

    public Biblioteca vincularLivro(Long livro_id, Long biblioteca_id) {
        Biblioteca biblioteca = validarBiblioteca(biblioteca_id);
        Livro livro = livroService.validarLivro(livro_id);
        biblioteca.setLivros(livro);
        return bibliotecaRepository.save(biblioteca);
    }
}
