package com.JPASpringProject.JPA.controller;

import com.JPASpringProject.JPA.model.Livro;
import com.JPASpringProject.JPA.service.LivroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/livro")
public class LivroController {
    @Autowired
    private LivroService livroService;

    @GetMapping("/listar")
    public List<Livro> listarLivros(){
        return livroService.listarLivros();
    }

    @PostMapping("/adicionar")
    public ResponseEntity<Livro> adicionarLivro(@RequestBody Livro livro){
        Livro auxLivro = livroService.adicionarLivro(livro);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxLivro);
    }



    @PutMapping("/vincular/{autor_id}/{livro_id}")
    public ResponseEntity<Livro> vincularAutor(@PathVariable Long autor_id, @PathVariable Long livro_id){
        Livro livro = livroService.vincularAutor(autor_id, livro_id);
        return ResponseEntity.status(HttpStatus.OK).body(livro);
    }






}
