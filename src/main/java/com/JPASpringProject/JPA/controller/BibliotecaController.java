package com.JPASpringProject.JPA.controller;

import com.JPASpringProject.JPA.model.Biblioteca;
import com.JPASpringProject.JPA.service.BibliotecaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/biblioteca")
public class BibliotecaController {
    @Autowired
    private BibliotecaService bibliotecaService;

    @GetMapping("/listar")
    public List<Biblioteca> listarBibliotecas(){
        return bibliotecaService.listarBibliotecas();
    }

    @PostMapping("/adicionar")
    public ResponseEntity<Biblioteca> adicionarBiblioteca(@RequestBody Biblioteca biblioteca){
        Biblioteca auxBiblioteca = bibliotecaService.adicionarBiblioteca(biblioteca);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxBiblioteca);
    }

    @PutMapping("/vincular/{livro_id}/{biblioteca_id}")
    public ResponseEntity<Biblioteca> vincularLivro(@PathVariable Long livro_id, @PathVariable Long biblioteca_id){
        Biblioteca biblioteca = bibliotecaService.vincularLivro(livro_id,biblioteca_id);
        return ResponseEntity.status(HttpStatus.OK).body(biblioteca);
    }
}
