package com.JPASpringProject.JPA.controller;

import com.JPASpringProject.JPA.model.Autor;
import com.JPASpringProject.JPA.service.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/autor")
public class AutorController {
    @Autowired
    private AutorService autorService;

    @GetMapping("/listar")
    public List<Autor> listarAutors(){
        return autorService.listarAutors();
    }

    @PostMapping("/adicionar")
    public ResponseEntity<Autor> adicionarAutor(@RequestBody Autor autor){
        Autor auxAutor = autorService.adicionarAutor(autor);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxAutor);
    }



}
