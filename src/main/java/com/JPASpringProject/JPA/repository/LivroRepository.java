package com.JPASpringProject.JPA.repository;

import com.JPASpringProject.JPA.model.Livro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LivroRepository extends JpaRepository<Livro, Long> {
}
