package com.JPASpringProject.JPA.repository;

import com.JPASpringProject.JPA.model.Biblioteca;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BibliotecaRepository extends JpaRepository<Biblioteca,Long> {
}
