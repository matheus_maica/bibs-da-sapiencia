package com.JPASpringProject.JPA.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bibliotecas")
public class Biblioteca{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;

    @OneToMany(mappedBy = "biblioteca")
    private List<Livro> livros;

    public void setLivros(Livro livro){
        livros.add(livro);
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
